﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public XmlReader xmlReader;
    public Dictionary<string, Job> AllJobs = new Dictionary<string, Job>();
    public List<string> OccupationTitles = new List<string>();
    public List<int> GameScenes;
    public Job CurrentJob;
    List<int> PreviouslyUsedJobIndexes = new List<int>();
    TMP_Text fTMP;
    TMP_Text mTMP;
    public float player1Money;
    public float player2Money;


    IEnumerator Start()
    {
        AllJobs = new Dictionary<string, Job>(xmlReader.AllJobs);
        OccupationTitles = new List<string>(xmlReader.OccupationTitles);

        fTMP =  GameObject.Find("LeftScore").GetComponent<TMP_Text>();
        mTMP =  GameObject.Find("RightScore").GetComponent<TMP_Text>();
        fTMP.text = "";
        mTMP.text = "";

        yield return NextGameLoad();
    }

    void Update()
    {
        if(Input.GetKey(KeyCode.A))
        {
            Job randomJob = null;
            string randomJobTitle = OccupationTitles[Random.Range(0, OccupationTitles.Count)];
            randomJob = AllJobs[randomJobTitle];

            GameObject.Find("JobTitle").GetComponent<TMP_Text>().text = randomJob.RandReadableOccupationTitle();
            GameObject.Find("LeftScore").GetComponent<TMP_Text>().text = "Score: $" + randomJob.FemaleWages;
            GameObject.Find("RightScore").GetComponent<TMP_Text>().text = "Score: $" + randomJob.MaleWages
                + "\n" + Mathf.Round(randomJob.FemaleWages/randomJob.MaleWages*100) + "%"
                + "\n" + randomJob.FemaleEmployees + ", " + randomJob.MaleEmployees;
            }
    }

    IEnumerator NextGameLoad()
    {
        GameObject.Find("JobTitle").GetComponent<TMP_Text>().text = "[[INSTRUCTIONS]]";
        fTMP.text = "<size=40>'FEMALE' PLAYER\n\nClick the green boxes to score points.\n\nUse arrow keys or mouse to move hand.";
        mTMP.text = "<size=40>'MALE' PLAYER\n\nPress [A] on the green boxes to score points.\n\nUse left thumb stuck to move hand.";

        yield return new WaitForSeconds(5f);
        GameObject.Find("JobTitle").GetComponent<TMP_Text>().text = "[[SELECTING JOB...]]";
        yield return new WaitForSeconds(2f);
        fTMP.text = "";
        mTMP.text = "";

        StartCoroutine(JobPicker(5f));
        yield return new WaitForSeconds(6f);


        while(true)
        {            
            GameObject.Find("LeftScreen").GetComponent<Image>().enabled = false;
            GameObject.Find("RightScreen").GetComponent<Image>().enabled = false;
            int randGame = GameScenes[Random.Range(0, GameScenes.Count)];
            AsyncOperation sceneLoad = SceneManager.LoadSceneAsync(randGame, LoadSceneMode.Additive);

            yield return sceneLoad;
            SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(randGame));
            yield return new WaitForSeconds(10f);

            GameObject.Find("LeftScreen").GetComponent<Image>().enabled = true;
            GameObject.Find("RightScreen").GetComponent<Image>().enabled = true;

            // round ended
            GameObject.Find("JobTitle").GetComponent<TMP_Text>().text = "[[ROUND ENDED]]";

            if(GameScore.CurrentGameScore != null)
            {
                GameScore gs = GameScore.CurrentGameScore;
                Debug.Log(gs.p1Score + " , " + gs.p2Score);


                fTMP.text = "<size=40>'FEMALE' PLAYER earned: " + gs.p1Score + " points.";
                yield return new WaitForSeconds(1f);
                mTMP.text = "<size=40>'MALE' PLAYER earned: " + gs.p2Score + " points.";
                yield return new WaitForSeconds(2f);

                if(gs.p1Score < gs.p2Score)
                {
                    fTMP.text += "\n\n<size=40>Or, " + Mathf.Round(((float)gs.p1Score)/((float)gs.p2Score)*100) + "% less points.";
                }
                if(gs.p1Score > gs.p2Score)
                {
                    fTMP.text += "\n\n<size=40>Or, " + Mathf.Round(((float)gs.p1Score)/((float)gs.p2Score)*100) + "% more points.";
                }

                yield return new WaitForSeconds(5f);

                float totalScore = gs.p1Score + gs.p2Score;
                float totalWages = CurrentJob.FemaleWages + CurrentJob.MaleWages;

                // WELL ACTUALLY
                GameObject.Find("JobTitle").GetComponent<TMP_Text>().text = "[[BUT...]]";
                yield return new WaitForSeconds(2f);
                GameObject.Find("JobTitle").GetComponent<TMP_Text>().text = CurrentJob.RandReadableOccupationTitle();

                fTMP.text = "<size=45>Women earn\n $" + CurrentJob.FemaleWages + " P.A.";
                yield return new WaitForSeconds(1f);
                mTMP.text = "<size=45>Men earn\n $" + CurrentJob.MaleWages + " P.A.";

                if(CurrentJob.FemaleWages < CurrentJob.MaleWages)
                {
                    yield return new WaitForSeconds(2f);
                    fTMP.text += "\n\n Or, " + Mathf.Round(CurrentJob.FemaleWages/CurrentJob.MaleWages*100f) + "% of men.";
                }
                if(CurrentJob.MaleWages < CurrentJob.FemaleWages)
                {
                    yield return new WaitForSeconds(2f);
                    fTMP.text += "\n\n Or, " + Mathf.Round((CurrentJob.FemaleWages/CurrentJob.MaleWages*100f)-100) + "% more than men.";
                }

                yield return new WaitForSeconds(5f);

                // player 1 should earn...
                float player1ActuallyScore = ((float)gs.p1Score) * (CurrentJob.FemaleWages/CurrentJob.MaleWages);
                float player2ActuallyScore = ((float)gs.p2Score) / (CurrentJob.FemaleWages/CurrentJob.MaleWages);

                Debug.Log(gs.p2Score + " / " + (CurrentJob.FemaleWages/CurrentJob.MaleWages));


                fTMP.text = "<size=40>Sooo.... 'FEMALE' PLAYER 'really' earned: " + player1ActuallyScore + " points.";
                yield return new WaitForSeconds(1f);
                mTMP.text = "<size=40>'MALE' PLAYER 'really' earned: " + player2ActuallyScore + " points.";
                yield return new WaitForSeconds(2f);

                player1Money+=player1ActuallyScore;
                player2Money+=player2ActuallyScore;

            }

            fTMP.text += "\n\n<size=40>current score: \n" + player1Money + " points.";
            yield return new WaitForSeconds(1f);
            mTMP.text += "\n\n<size=40>current score: \n" + player2Money + " points.";
            yield return new WaitForSeconds(4f);

            fTMP.text = "";
            mTMP.text = "";

            GameObject.Find("JobTitle").GetComponent<TMP_Text>().text = "[[SELECTING JOB...]]";
            yield return new WaitForSeconds(2f);

            StartCoroutine(JobPicker(2f));
            yield return new WaitForSeconds(3f);

            SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(0));
            sceneLoad = SceneManager.UnloadSceneAsync(SceneManager.GetSceneByBuildIndex(randGame));
            yield return sceneLoad;

        }
    }

    IEnumerator JobPicker(float duration)
    {
        float t = 0;
        Job randomJob = null;
        int jobindex = -1;
        duration = duration - 1f;
        while(t<1f)
        {
            jobindex = Random.Range(0, OccupationTitles.Count);
            while(PreviouslyUsedJobIndexes.Contains(jobindex))
            {
                jobindex = Random.Range(0, OccupationTitles.Count);
            }
            string randomJobTitle = OccupationTitles[jobindex];
            randomJob = AllJobs[randomJobTitle];

            GameObject.Find("JobTitle").GetComponent<TMP_Text>().text = randomJob.RandReadableOccupationTitle();

            /*
            GameObject.Find("LeftScore").GetComponent<TMP_Text>().text = "Score: $" + randomJob.FemaleWages;
            GameObject.Find("RightScore").GetComponent<TMP_Text>().text = "Score: $" + randomJob.MaleWages
                + "\n" + Mathf.Round(randomJob.FemaleWages/randomJob.MaleWages*100) + "%"
                + "\n" + randomJob.FemaleEmployees + ", " + randomJob.MaleEmployees;
            */

            t+=Time.deltaTime;
            yield return 0;
        }

        yield return new WaitForSeconds(1f);

        /*
        fTMP.text = "<size=40>Women earn\n $" + randomJob.FemaleWages + " P.A.";
        yield return new WaitForSeconds(1f);
        mTMP.text = "<size=40>Men earn\n $" + randomJob.MaleWages + " P.A.";


        if(randomJob.FemaleWages < randomJob.MaleWages)
        {
            yield return new WaitForSeconds(2f);
            fTMP.text += "\n\n Or, " + Mathf.Round(randomJob.FemaleWages/randomJob.MaleWages*100f) + "% of men.";
        }
        if(randomJob.MaleWages < randomJob.FemaleWages)
        {
            yield return new WaitForSeconds(2f);
            fTMP.text += "\n\n Or, " + Mathf.Round((randomJob.FemaleWages/randomJob.MaleWages*100f)-100) + "% more than men.";
        }
*/
        yield return new WaitForSeconds(duration - 1f);

        fTMP.text = "";
        mTMP.text = "";

        CurrentJob = randomJob;
        PreviouslyUsedJobIndexes.Add(jobindex);
    }

    public void LoadScene(int sceneIndex)
    {

    }
}
