﻿using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Job
{
    public string Occupation;
    public float MaleEmployees;
    public float FemaleEmployees;
    public float MaleWages;
    public float FemaleWages;
    public bool DataComplete = false;

    public Job(string name, float maleEmployees, float femaleEmployees, float maleWages, float femaleWages)
    {
        Occupation = name;
        MaleEmployees = maleEmployees;
        FemaleEmployees = femaleEmployees;
        MaleWages = maleWages;
        FemaleWages = femaleWages;

        DataComplete = true;

       // Debug.Log("Rand readable title: " + RandReadableOccupationTitle());
    }

    public Job(string name)
    {
        Occupation = name;

      //  Debug.Log("Rand readable title: " + RandReadableOccupationTitle());
    }

    public string RandReadableOccupationTitle()
    {
        string[] possibleTitles = Occupation.Split(";"[0]);
        string pickedTitle = possibleTitles[Random.Range(0, possibleTitles.Length)];

     //   Debug.Log("picked: " + pickedTitle);

        return Regex.Replace(pickedTitle, @"[^A-Za-z -/]+", string.Empty).Trim();
    }

    public void AddFemaleData(float femaleEmployees, float femaleWages)
    {
        FemaleEmployees = femaleEmployees;
        FemaleWages = femaleWages;
        CheckDataComplete();
    }

    public void AddMaleData(float maleEmployees, float maleWages)
    {
        MaleEmployees = maleEmployees;
        MaleWages = maleWages;
        CheckDataComplete();
    }

    public override string ToString()
    {
        return string.Format("[Job] " + Occupation + ": " + MaleEmployees + " at $" + MaleWages + " vs " + FemaleEmployees + " at $" + FemaleWages);
    }

    protected void CheckDataComplete()
    {
        if(MaleEmployees > 0 && MaleWages > 0 && FemaleWages > 0 && FemaleEmployees > 0)
        {
            DataComplete = true;
        }
    }
}
