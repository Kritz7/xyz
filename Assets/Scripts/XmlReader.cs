﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XmlReader : MonoBehaviour {

    public TextAsset CsvFile;
    public Dictionary<string, Job> AllJobs = new Dictionary<string, Job>();
    public List<string> OccupationTitles = new List<string>();
    
	// Use this for initialization
    void Awake ()
    {
        CVSReader(CsvFile);
	}
	
    void CVSReader(TextAsset file)
    {
        AllJobs = CvsSplitter(CsvFile.text);

        // Make sure jobs are complete
      //  Debug.Log("There are " + AllJobs.Count + " jobs. Removing incomplete jobs.");
        List<Job> JobsToBeRemoved = new List<Job>();
        foreach(var jobd in AllJobs)
        {
            if(!jobd.Value.DataComplete)
            {
                JobsToBeRemoved.Add(jobd.Value);
            }
        }

        while(JobsToBeRemoved.Count > 0)
        {
            AllJobs.Remove(JobsToBeRemoved[0].Occupation);
            OccupationTitles.Remove(JobsToBeRemoved[0].Occupation);
            JobsToBeRemoved[0] = null;
            JobsToBeRemoved.RemoveAt(0);
        }

        Debug.Log("There are now " + AllJobs.Count);
     }

    Dictionary<string, Job> CvsSplitter(string text)
    {
        Dictionary<string, Job> jobsRaw = new Dictionary<string, Job>();

        string[] lines = text.Split("\n"[0]);
        for(int i=0; i<lines.Length; i++)
        {
            List<string> lineColumns = new List<string>(lines[i].Split("^"[0]));
            Job newJob = new Job(lineColumns[0]);

            if(!OccupationTitles.Contains(lineColumns[0]))
            {
                OccupationTitles.Add(lineColumns[0]);
            }

            if(jobsRaw.ContainsKey(lineColumns[0]))
            {
                newJob = jobsRaw[lineColumns[0]];

                if(lineColumns[1].ToLower() == "female")
                {
                    float Employees,Wages;
                    bool parseSuccess = float.TryParse(lineColumns[2], out Employees);
                    parseSuccess = parseSuccess && float.TryParse(lineColumns[3], out Wages);

                    if(parseSuccess)
                    {
                        newJob.AddFemaleData(float.Parse(lineColumns[2]), float.Parse(lineColumns[3]));
                    }
                }
                if(lineColumns[1].ToLower() == "male")
                {
                    float Employees,Wages;
                    bool parseSuccess = float.TryParse(lineColumns[2], out Employees);
                    parseSuccess = parseSuccess && float.TryParse(lineColumns[3], out Wages);

                    if(parseSuccess)
                    {
                        newJob.AddMaleData(float.Parse(lineColumns[2]), float.Parse(lineColumns[3]));
                    }
                }

                // At this point the job data should be filled for newjob.
            }
            else
            {
                if(lineColumns[1].ToLower() == "female")
                {
                    float Employees,Wages;
                    bool parseSuccess = float.TryParse(lineColumns[2], out Employees);
                    parseSuccess = parseSuccess && float.TryParse(lineColumns[3], out Wages);

                    if(parseSuccess)
                    {
                        newJob.AddFemaleData(float.Parse(lineColumns[2]), float.Parse(lineColumns[3]));
                    }
                }
                if(lineColumns[1].ToLower() == "male")
                {
                    float Employees,Wages;
                    bool parseSuccess = float.TryParse(lineColumns[2], out Employees);
                    parseSuccess = parseSuccess && float.TryParse(lineColumns[3], out Wages);

                    if(parseSuccess)
                    {
                        newJob.AddMaleData(float.Parse(lineColumns[2]), float.Parse(lineColumns[3]));
                    }
                }

                // This job is going to be half filled
                jobsRaw.Add(lineColumns[0], newJob);
            }
        }

        return jobsRaw;
    }
}
