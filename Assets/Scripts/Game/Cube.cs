﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour {

    public PlayerControls.ControllerType playerSide;
    public bool shouldSelect = false;

    public void Use()
    {
        if(shouldSelect)
        {
            if(playerSide == PlayerControls.ControllerType.keyboard)
                GameScore.CurrentGameScore.AddScoreP1(1);

            if(playerSide == PlayerControls.ControllerType.gamepad)
                GameScore.CurrentGameScore.AddScoreP2(1);

            CubeGrid.CubeGrids[playerSide].selectionMade = true;
            GetComponent<Renderer>().material = CubeGrid.CubeGrids[playerSide].dontSelect;
        }

        shouldSelect = false;
    }
}
