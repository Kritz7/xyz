﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameScore : MonoBehaviour
{
    public static GameScore CurrentGameScore;
    public int p1Score;
    public int p2Score;

    void Awake()
    {
        CurrentGameScore = this;
    }

    public void AddScoreP1(int score)
    {
        p1Score += score;
    }

    public void AddScoreP2(int score)
    {
        p2Score += score;
    }
}
