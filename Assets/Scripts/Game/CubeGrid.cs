﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeGrid : MonoBehaviour
{
    public static Dictionary<PlayerControls.ControllerType, CubeGrid> CubeGrids;
    public PlayerControls.ControllerType playerSide;
    public GameObject Up;
    public GameObject Down;
    public GameObject Left;
    public GameObject Right;

    public Material SelectMe;
    public Material dontSelect;

    public bool selectionMade = false;

    enum direction
    {
        Up, Down, Left, Right
    }
    direction currentDirection = direction.Up;

    IEnumerator Start()
    {
        if(CubeGrids == null)
        {
            CubeGrids = new Dictionary<PlayerControls.ControllerType, CubeGrid>();
        }

        if(CubeGrids.ContainsKey(playerSide))
        {
            CubeGrids[playerSide] = this;
        }
        else
        {
            CubeGrids.Add(playerSide, this);
        }

        foreach(Cube c in GetComponentsInChildren<Cube>())
        {
            c.playerSide = playerSide;
        }

        yield return Minigame();
    }

    IEnumerator Minigame()
    {
        yield return new WaitForSeconds(0.5f);

        while(true)
        {
            int randDirection = Random.Range(0,4);
            while(randDirection == (int)currentDirection)
            {
                randDirection = Random.Range(0,4);
            }

            Toggle(currentDirection, (direction)randDirection);
            currentDirection = (direction)randDirection;

            yield return WaitForSelection(2f);
        }
    }

    IEnumerator WaitForSelection(float duration)
    {
        float t =0;

        while(t<duration && selectionMade == false)
        {
            t+=Time.deltaTime;
            yield return 0;
        }

        selectionMade = false;
    }

    void Toggle(direction old, direction newdir)
    {
        Up.GetComponent<Renderer>().material = dontSelect;
        Down.GetComponent<Renderer>().material = dontSelect;
        Left.GetComponent<Renderer>().material = dontSelect;
        Right.GetComponent<Renderer>().material = dontSelect;

        foreach(Cube c in GetComponentsInChildren<Cube>())
        {
            c.shouldSelect = false;
        }


        if(newdir == direction.Up)
        {
            Up.GetComponent<Renderer>().material = SelectMe;
            Up.GetComponent<Cube>().shouldSelect = true;
        }
        if(newdir == direction.Down)
        {
            Down.GetComponent<Renderer>().material = SelectMe;
            Down.GetComponent<Cube>().shouldSelect = true;
        }
        if(newdir == direction.Left)
        {
            Left.GetComponent<Renderer>().material = SelectMe;
            Left.GetComponent<Cube>().shouldSelect = true;
        }
        if(newdir == direction.Right)
        {
            Right.GetComponent<Renderer>().material = SelectMe;
            Right.GetComponent<Cube>().shouldSelect = true;
        }
    }
}
