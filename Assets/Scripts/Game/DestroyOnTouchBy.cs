﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnTouchBy : MonoBehaviour {

    public string enemyName = "";
    public float destroyAfterSeconds = 1f;
    public bool shrink = false;
    bool isBeingDestroyed = false;
    public int scoreReward = 0;

    void OnCollisionEnter(Collision c)
    {
        if(c.gameObject.name.ToLower().Contains(enemyName.ToLower()))
        {
            Debug.Log("Hit by broom!");
            if(!isBeingDestroyed)
            {
                StartCoroutine(Destroying(destroyAfterSeconds));

                if(c.gameObject.GetComponent<PlayerControls>())
                {
                    if(c.gameObject.GetComponent<PlayerControls>().controller == PlayerControls.ControllerType.keyboard)
                    {
                        Debug.Log("Score 1 !");
                        GameScore.CurrentGameScore.AddScoreP1(scoreReward);
                    }
                    else if(c.gameObject.GetComponent<PlayerControls>().controller == PlayerControls.ControllerType.gamepad)
                    {
                        GameScore.CurrentGameScore.AddScoreP2(scoreReward);
                    }
                }
            }
        }
    }

    IEnumerator Destroying(float destroyDuration)
    {
        isBeingDestroyed = true;
        float t = 0;
        while(t<destroyDuration)
        {
            if(shrink)
                transform.localScale = Vector3.Lerp(transform.localScale, Vector3.zero, t/destroyDuration);
            
            t+=Time.deltaTime;  
            yield return 0;
        }
        Debug.Log("done.");
        GameObject.Destroy(this.gameObject);
    }
}
