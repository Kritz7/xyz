﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour
{
    public enum ControllerType
    {
        keyboard,
        gamepad
    }
    public ControllerType controller;
    public float speed = 10f;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

	// Update is called once per frame
	void Update ()
    {
        if(Input.GetMouseButton(0))
            Cursor.lockState = CursorLockMode.Locked;

        Vector2 moveDir = Vector2.zero;
        bool use = false;

        if(controller == ControllerType.keyboard)
        {
          //  moveDir.x = Input.GetAxis("Horizontal");
           // moveDir.y = Input.GetAxis("Vertical");

            if(moveDir.x == 0)
                moveDir.x = Input.GetAxis("Mouse X");
            if(moveDir.y == 0)
                moveDir.y = Input.GetAxis("Mouse Y");

            use = Input.GetButton("Fire1");
            if(use == false)
                use = Input.GetMouseButton(0);
        }

        if(controller == ControllerType.gamepad)
        {
            moveDir.x = Input.GetAxis("PadHoz");
            moveDir.y = -Input.GetAxis("PadVert");

            if(Mathf.Abs(moveDir.x)<0.4f) moveDir.x = 0;
            if(Mathf.Abs(moveDir.y)<0.4f) moveDir.y = 0;

            use = Input.GetButton("Fire2");
        }

        transform.position += new Vector3(moveDir.x, moveDir.y, 0f) * speed * Time.deltaTime;

        if(use)
        {
            
            RaycastHit[] hits = Physics.RaycastAll(transform.position, transform.forward, 100f);
            if(hits.Length>0)
            {
               
                foreach(RaycastHit hit in hits)
                {
                    if(hit.transform.gameObject.GetComponent<Cube>())
                        hit.transform.gameObject.GetComponent<Cube>().Use();
                }
            }
        }
	}
}
