﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerForce : MonoBehaviour
{
    public GameObject prefab;
    public float spawnPerSecond = 1f;
    public int objectsPerSpawn = 1;
    public Vector3 spawnForce;
    public Vector3 spawnForceRandom;
    public float spawnRangeX = 0f;
    public float spawnRangeY = 0f;

	// Use this for initialization
	void Start ()
    {
        StartCoroutine(Spawner());
	}

    IEnumerator Spawner()
    {
        while(true)
        {
            for(int i=0; i<objectsPerSpawn; i++)
            {
                SpawnObject();
            }
            yield return new WaitForSeconds(spawnPerSecond) ;
        }
    }

    void SpawnObject()
    {
        Vector3 randSpawnPos = Random.insideUnitSphere * spawnRangeX;
        randSpawnPos.z = 0f;
        randSpawnPos.y = Random.Range(-spawnRangeY, spawnRangeY);

        GameObject newObj = GameObject.Instantiate(prefab, transform.position + randSpawnPos, Quaternion.identity);
        if(prefab.GetComponent<Rigidbody>())
        {
            Rigidbody rb = newObj.GetComponent<Rigidbody>();
            rb.AddForce(spawnForce + new Vector3(
                Random.Range(-spawnForceRandom.x, spawnForceRandom.x),
                Random.Range(-spawnForceRandom.y, spawnForceRandom.y),
                Random.Range(-spawnForceRandom.z, spawnForceRandom.z)));
        }
    }
}
