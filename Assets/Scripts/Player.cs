﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public static Dictionary<string, Player> Players;
    public string Gender;
    public float CurrentMoney = 0;
    public float BiasPercentage = 0;
    public int TechnicalWins = 0;
    public int WinWins = 0;

    void Awake()
    {
        if(Players == null)
        {
            Players = new Dictionary<string, Player>();
        }

        Players.Add(Gender, this);
    }

    public void AddResults(float moneyEarned, float bias, bool technicalWin, bool Won)
    {
        CurrentMoney += moneyEarned;
        BiasPercentage += bias;

        if(technicalWin) TechnicalWins++;
        if(Won) WinWins++;
    }
}
